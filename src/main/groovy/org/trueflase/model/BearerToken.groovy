package org.trueflase.model

import groovy.transform.CompileStatic

/**
 *
 * @author Michał Szturc
 */
@CompileStatic
class BearerToken {

    String access_token

    Integer expires_in

    String token_type
}
