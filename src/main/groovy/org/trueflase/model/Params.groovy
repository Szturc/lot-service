package org.trueflase.model

import groovy.transform.CompileStatic

/**
 *
 * @author Michał Szturc
 */
@CompileStatic
class Params {

    String cabinClass

    String market

    List<String> departureDate

    String returnDate

    List<String> origin

    String tripType

    Integer adt

    List<String> destination





}
