package org.trueflase.model


import groovy.transform.CompileStatic
import groovy.transform.ToString

/**
 *
 * @author Michał Szturc
 */
@CompileStatic
@ToString
class AvailabilityRequest {

    Params params

    Options options
}
