package org.trueflase.model

import groovy.transform.CompileStatic

/**
 *
 * @author Michał Szturc
 */
@CompileStatic
class Price {
    BigDecimal price

    BigDecimal basePrice

    BigDecimal tax

    String currency

}
