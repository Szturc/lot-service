package org.trueflase.model

import groovy.transform.CompileStatic

/**
 *
 * @author Michał Szturc
 */
@CompileStatic
class AvailabilityResponse {


    String status

    List errors
}
