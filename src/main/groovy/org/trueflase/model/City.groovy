package org.trueflase.model

import groovy.transform.CompileStatic

/**
 *
 * @author Michał Szturc
 */
@CompileStatic
class City {

    String city

    String iataCode
}
