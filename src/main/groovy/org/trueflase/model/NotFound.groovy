package org.trueflase.model

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 *
 * @author Michał Szturc
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
class NotFound extends RuntimeException {

    NotFound(String message) {
        super(message)
    }
}
