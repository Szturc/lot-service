package org.trueflase

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.client.RestTemplate

/**
 *
 * @author Michał Szturc
 */
@SpringBootApplication
class LotApplication {

    static void main(String[] args) {
        SpringApplication.run(LotApplication, args)
    }

    @Bean
    RestTemplate restTemplate () {
        new RestTemplate()
    }
}
