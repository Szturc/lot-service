package org.trueflase.services

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.trueflase.model.Airport

import javax.annotation.PostConstruct

/**
 *
 * @author Michał Szturc
 */
@Component
@CompileStatic
class AirportsCache {

    private final List<Airport> airports = new LinkedList<>()

    private final LotApiClient lotApiClient

    @Autowired
    AirportsCache(LotApiClient lotApiClient) {
        this.lotApiClient = lotApiClient
    }

    @PostConstruct
    init() {
        refreshAirports()
    }

    void refreshAirports() {
        this.airports.clear()
        airports.addAll(lotApiClient.allAirports)
    }


    List<Airport> getAirports() {
        return airports
    }
}
