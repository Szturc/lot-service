package org.trueflase.services

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import org.trueflase.model.*

/**
 *
 * @author Michał Szturc
 */
@Service
@Slf4j
@CompileStatic
class LotApiClient {

    private final String apiToken
    private final String apiSecret
    private final String lotBaseURI
    private final RestTemplate restTemplate

    @Autowired
    LotApiClient(@Value('${lot.X-Api-Key}') String apiToken,
                 @Value('${lot.secret-key}') String apiSecret,
                 @Value('${lot.base-url}') String lotBaseURI,
                 RestTemplate restTemplate) {
        this.apiToken = apiToken
        this.apiSecret = apiSecret
        this.lotBaseURI = lotBaseURI
        this.restTemplate = restTemplate
    }

    String apiLogin() {
        def headers = new HttpHeaders()
        headers.setContentType(MediaType.APPLICATION_JSON)
        headers.set("X-Api-Key", apiToken)

        def body = [
                "secret_key": apiSecret,
                "params"    : [
                        "market"  : "PL",
                        "language": "PL"
                ]
        ]
        log.info(requestURI('/auth/token/get').toString())
        this.restTemplate.exchange(
                requestURI('/auth/token/get'),
                HttpMethod.POST,
                new HttpEntity(body, headers),
                BearerToken).body.access_token
    }

    String refreshToken(String bearerToken) {
        def headers = new HttpHeaders()
        headers.setContentType(MediaType.APPLICATION_JSON)
        headers.set("X-Api-Key", apiToken)
        headers.setBearerAuth(bearerToken)
        this.restTemplate.exchange(requestURI("/auth/token/refresh"),
                HttpMethod.POST,
                new HttpEntity(headers),
                BearerToken).body.access_token
    }

    List<Airport> getAllAirports() {
        def headers = new HttpHeaders()
        headers.setContentType(MediaType.APPLICATION_JSON)
        headers.set("X-Api-Key", apiToken)
        restTemplate.exchange(requestURI("/common/airports/get"),
                HttpMethod.GET,
                new HttpEntity(headers),
                List).body
    }

    Map getFlightAvailability(String token, AvailabilityRequest request) {
        request.options = new Options(fromCache: true, fareType: "ALL")


        def headers = new HttpHeaders()
        headers.setContentType(MediaType.APPLICATION_JSON)
        headers.set("X-Api-Key", apiToken)
        headers.setBearerAuth(token)

        try {
            def result = restTemplate.exchange(requestURI("/booking/availability"),
                    HttpMethod.POST,
                    new HttpEntity(request, headers),
                    Map).body

            result
        } catch (Exception exe) {
            throw new NotFound(exe.message)
        }
    }

    private URI requestURI(String path) {
        UriComponentsBuilder
                .fromUriString(lotBaseURI)
                .path(path)
                .build()
                .toUri()
    }

    private String availabilityBodyRequest(AvailabilityRequest availabilityRequest) {
        [
                "params" : [
                        "cabinClass"   : "E",
                        "market"       : "PL",
                        "departureDate": ["22022019"],
                        "returnDate"   : "25022019",
                        "origin"       : ["WAW"],
                        "tripType"     : "R",
                        "adt"          : 1,
                        "destination"  : ["CDG"]
                ],
                "options": [
                        "fromCache": true,
                        "fareType" : ["ALL"]
                ]
        ]
    }
}
