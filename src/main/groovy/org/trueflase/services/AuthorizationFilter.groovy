package org.trueflase.services

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter

import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 *
 * @author Michał Szturc
 */
@CompileStatic
@Slf4j
class AuthorizationFilter extends BasicAuthenticationFilter {

    private final  LotApiClient lotApiClient
    AuthorizationFilter(AuthenticationManager authenticationManager, LotApiClient lotApiClient) {
        super(authenticationManager)
        this.lotApiClient = lotApiClient
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        def context = SecurityContextHolder.context.authentication

        if (context) {
            lotApiClient.refreshToken(context.principal.toString())
        }else {
            def token = lotApiClient.apiLogin()
            def auth = new UsernamePasswordAuthenticationToken(token, null, null)
            SecurityContextHolder.context.authentication = auth
        }
    }
}
