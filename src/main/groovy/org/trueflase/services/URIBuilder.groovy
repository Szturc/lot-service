package org.trueflase.services

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder

/**
 *
 * @author Michał Szturc
 */
@Component
@CompileStatic
@Slf4j
class URIBuilder {

    private final String lotBaseURI

    @Autowired
    URIBuilder(@Value('${lot.base-url}') String lotBaseURIString) {
        this.lotBaseURI = lotBaseURIString
    }

   private URI requestURI() {
        UriComponentsBuilder
                .fromUriString(lotBaseURI)
                .path("/auth/token/get")
                .build()
                .toUri()
    } // /auth/token/refresh
}
