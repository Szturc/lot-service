package org.trueflase.configuration

import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.trueflase.services.AuthorizationFilter
import org.trueflase.services.LotApiClient

/**
 *
 * @author Michał Szturc
 */
@Configuration
@CompileStatic
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private final LotApiClient lotApiClient

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .httpBasic()
                .disable()
                .addFilter(new AuthorizationFilter(authenticationManager(), lotApiClient))
                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, '/api/search')
//                   .permitAll()
                .anyRequest()
                .authenticated()
        // @formatter:on

    }
}
