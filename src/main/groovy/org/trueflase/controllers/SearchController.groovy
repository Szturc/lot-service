package org.trueflase.controllers

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.trueflase.model.Airport
import org.trueflase.model.AvailabilityRequest
import org.trueflase.model.Params
import org.trueflase.services.AirportsCache
import org.trueflase.services.LotApiClient

import javax.servlet.http.HttpServletRequest

/**
 *
 * @author Michał Szturc
 */
@RestController
@CompileStatic
@Slf4j
@RequestMapping("/api")
class SearchController {

    private final LotApiClient lotApiClient
    private final AirportsCache airportsCache

    @Autowired
    SearchController(LotApiClient lotApiClient, AirportsCache airportsCache) {
        this.lotApiClient = lotApiClient
        this.airportsCache = airportsCache
    }

    @GetMapping
    String test() {
        lotApiClient.apiLogin()
    }

    @GetMapping("/refresh")
    String test2(@RequestParam("token") String token) {
        lotApiClient.refreshToken(token)
    }

    @GetMapping("/availability")
    Map flightAvailability(
            @RequestParam("departureDate") String departure,
            @RequestParam(value = "returnDate", required = false) String returnDate,
            @RequestParam(value = "cabinClass", required = false, defaultValue = "E") String cabinClass,
            @RequestParam(value = 'market', required = false, defaultValue = "PL") String market,
            @RequestParam(value = "origin", required = true) List<String> origin,
            @RequestParam(value = "destination", required = false) List<String> destination,
            @RequestParam(value = "adultsCount", required = false) Integer adultsCount,
            @RequestParam(value = "tripType", required = false) String tripType,
            HttpServletRequest httpRequest) {
        def token = httpRequest.getHeader("authorization").split(" ").last()
        def request = new AvailabilityRequest(params: new Params(origin: origin, destination: destination,
                departureDate: [departure.toString()], returnDate: returnDate.toString(), tripType: "R",
                cabinClass: cabinClass, adt: adultsCount, market: market))
        lotApiClient.getFlightAvailability(token, request)
    }

    @GetMapping("/airports")
    List<Airport> airports() {
        airportsCache.getAirports()
    }
}
