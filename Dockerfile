FROM azul/zulu-openjdk-debian:8

COPY ./org/ /app/org/
COPY ./BOOT-INF/lib/ /app/BOOT-INF/lib/
COPY ./BOOT-INF/classes/org/ /app/BOOT-INF/classes/org/
#COPY ./BOOT-INF/classes/config /app/BOOT-INF/classes/config

COPY ./META-INF /app/META-INF/

WORKDIR /app

ENTRYPOINT ["java", "-Dfile.encoding=UTF-8", "-Djava.security.egd=file:/dev/./urandom", "org.springframework.boot.loader.JarLauncher"]